# README #

'Default (OSX).sublime-keymap' file resides in your ~/Library/Application Support/Sublime Text 3/Packages/User directory. Note that these are really your User keybindings - the "default" label can be misleading. If you don't have one, simply create it (or download this one to that directory). 

Your **User** default bindings can be accessed via Sublime Text 3 from the SublimeText menu:
Sublime Text > Preferences > Key Bindings - User

Useful references for extending your keybindings:
http://docs.sublimetext.info/en/latest/reference/key_bindings.html?highlight=keymap
http://www.sublimetext.com/docs/commands
http://docs.sublimetext.info/en/latest/reference/keyboard_shortcuts_osx.html

Sublime Text **Default** bindings can be accessed via Sublime Text 3 from the SublimeText menu:

Sublime Text > Preferences > Key Bindings - Default